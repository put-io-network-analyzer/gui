# Network Analyzer app

Single page application created for  

## Prerequisites

1. Nodejs >= 10.0.0
2. NPM or Yarn

## How to run

1. Run the Network Analyzer REST API
2. Run the Network Analyzer Gui ([See Project Setup](#project-setup))

## Project setup
```
npm install
```
or
```
yarn
```

### Compiles and hot-reloads for development / Run GUI on local server
```
npm run serve
```
or
```
yarn serve
```

### Compiles and minifies for production
```
npm run build
```
or
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
